package utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ReusableClass  {
    WebDriver driver;
    static ExtentTest test;
    static ExtentReports report;
    int count=0;
    int countScreen=0;

    String reportLocation = "";
public void start(){
    ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
    driver = new ChromeDriver();
}
public void report(String testName,String testID){
    reportLocation = System.getProperty("user.dir")+ File.separator+"target"+File.separator+testName+File.separator+testID;
    report = new ExtentReports(reportLocation+File.separator+"ExtentReportResults.html");
    test = report.startTest(testName);
}
public void launch() throws Exception {
    driver.manage().deleteAllCookies();
    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    driver.get("https://www.way2automation.com/angularjs-protractor/webtables/");
    driver.manage().window().maximize();
    takeSnapShot(driver,reportLocation+File.separator+countScreenShot()+".launch application.png");


    if (driver.getTitle().equalsIgnoreCase("Protractor practice website - webTables") && driver.findElement(By.xpath("/html/body/table/thead/tr[3]/th[1]/span")).getText().trim().equalsIgnoreCase("First Name")){
        test.log(LogStatus.PASS, "Navigated to the Home page");
    }else {
        test.log(LogStatus.FAIL, "Test Failed to launch application");
    }
}
public void reportStatus(String status, String comments){
    if (status.equalsIgnoreCase("pass"))
    test.log(LogStatus.PASS, comments);
    else
        test.log(LogStatus.FAIL, comments);
}
public  void addUserBtn() throws Exception {
    if (driver.getTitle().equalsIgnoreCase("Protractor practice website - webTables") && driver.findElement(By.xpath("/html/body/table/thead/tr[3]/th[1]/span")).getText().trim().equalsIgnoreCase("First Name")){
        driver.findElement(By.xpath("/html/body/table/thead/tr[2]/td/button")).click();
        takeSnapShot(driver,reportLocation+File.separator+countScreenShot()+".add user.png");
    }
}
public void addUserInput(String firstName,String lastName,String password,String customer,String role,String email,String cell) throws Exception {
    if (driver.findElement(By.xpath("/html/body/div[3]/div[1]/h3")).getText().trim().equalsIgnoreCase("Add User")){
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[1]/td[2]/input")).sendKeys(firstName);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[2]/td[2]/input")).sendKeys(lastName);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[3]/td[2]/input")).sendKeys(userGenerate(firstName,lastName));
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[4]/td[2]/input")).sendKeys(password);
        customerType(customer);
        Select roleDropdown = new Select(driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[6]/td[2]/select")));
        Thread.sleep(1000);
        roleDropdown.selectByVisibleText(role);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[7]/td[2]/input")).sendKeys(email);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[8]/td[2]/input")).sendKeys(cell);
        Thread.sleep(1000);
        takeSnapShot(driver,reportLocation+File.separator+countScreenShot()+".add user input.png");
        driver.findElement(By.xpath("/html/body/div[3]/div[3]/button[2]")).click();
    }

}
public String userGenerate(String firstName,String lastName) throws InterruptedException {
    Random rand = new Random();
    int number = rand.nextInt(999999);
    String username = firstName.substring(0,1).concat(lastName.substring(0,1)).concat(String.format("%06d",number));

    return username;
}
public void customerType(String customer){
    if (customer.equalsIgnoreCase("company aaa")){
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[5]/td[2]/label[1]/input")).click();
    }else if(customer.equalsIgnoreCase("company bbb")){
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[5]/td[2]/label[2]/input")).click();
    }else {

    }
}
public void confirmUserAdded(String firstName,String lastName) throws Exception {
    if (driver.findElement(By.xpath("/html/body/table/thead/tr[3]/th[1]/span")).getText().trim().equalsIgnoreCase("First Name")){

    int i = 1;
        boolean control=true;
                do{
                    if(isElementVisible("/html/body/table/tbody/tr["+i+"]/td[1]",2)){
                        i++;
                        control = true;
                    }else{
                        control = false;
                    }
                }while (control == true);
                breakloop:
                for (int x=1;x<i;i++){
                    if (driver.findElement(By.xpath("/html/body/table/tbody/tr["+x+"]/td[1]")).getText().equalsIgnoreCase(firstName) &&
                    driver.findElement(By.xpath("/html/body/table/tbody/tr["+x+"]/td[2]")).getText().equalsIgnoreCase(lastName)){
                        test.log(LogStatus.PASS, "user has been created");
                        takeSnapShot(driver,reportLocation+File.separator+countScreenShot()+".user added complete.png");
                        break breakloop;
                    }else{
                        test.log(LogStatus.FAIL, "Test Failed to add a user");
                    }
                }
        takeSnapShot(driver,reportLocation+File.separator+countScreenShot()+".end test.png");
    }

}
    public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{

        TakesScreenshot screenshot =((TakesScreenshot)webdriver);

        File SrcFile=screenshot.getScreenshotAs(OutputType.FILE);

        File DestFile=new File(fileWithPath);

        FileUtils.copyFile(SrcFile, DestFile);
    }
    public String count(){

        count++;
        String value = "";
        if (count<10){
            value = "0"+String.valueOf(count);
        }
        else{
            value = String.valueOf(count);
        }
        return value;
    }
    public String countScreenShot(){

        countScreen++;
        String value = "";
        if (countScreen<10){
            value = "0"+String.valueOf(countScreen);
        }
        else{
            value = String.valueOf(countScreen);
        }
        return value;
    }

    public boolean isElementVisible(String xpath, int retryAmount) {
        for (int i = 0; i < retryAmount; i++) {
            if (isElementVisible(xpath)) {
                  System.out.println(xpath + " retried: " + ++i);
                return true;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
    private boolean isElementVisible(String xpath) {
        try {
            driver.findElement(By.xpath(xpath));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
    public void close(){
        report.endTest(test);
        report.flush();
        driver.quit();
    }


}
