package api;

import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class RandomImage {

    @Test
    public void test(){

                given()
                        .get("https://dog.ceo/api/breeds/image/random").then().assertThat().statusCode(HttpStatus.SC_OK)
                        .log().all();
    }
}
