package api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.given;

@RunWith(Parameterized.class)
public class ListAllSubBreedImages {
    private String breed;
    private String subBreed;
    public ListAllSubBreedImages(String breed,String subBreed){
        this.breed = breed;
        this.subBreed = subBreed;
    }
    @Test
    public void test(){
                String urlSubBreedImages = "https://dog.ceo/api/breed/"+breed+"/"+subBreed+"/images";
                given()
                        .get(urlSubBreedImages).then().statusCode(200)
                        .log().all();
    }
    @Parameterized.Parameters
    public static String[][] dp() throws IOException {

        String csvDataFilePath = System.getProperty("user.dir")+ File.separator+"src"+ File.separator+"test"+ File.separator+"resources"+ File.separator+"data"+ File.separator+"listAllSubBreedImages.csv";
        String[][] dataFromCSV = utils.CSVDataReaders.getCSVData(csvDataFilePath);
        return dataFromCSV;
    }
}
