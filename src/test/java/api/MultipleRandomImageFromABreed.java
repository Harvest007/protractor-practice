package api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.given;

@RunWith(Parameterized.class)
public class MultipleRandomImageFromABreed {
    private String breed ;
    private String numberOfRandomBreeds;

    public MultipleRandomImageFromABreed(String breed, String numberOfRandomBreeds){
        this.breed = breed;
        this.numberOfRandomBreeds = numberOfRandomBreeds;
    }
    @Test
    public void test(){
                    String urlBreed = "https://dog.ceo/api/breed/"+breed+"/images/random/".concat(numberOfRandomBreeds);
                given()
                        .get(urlBreed).then().statusCode(200)
                        .log().all();
    }
    @Parameterized.Parameters
    public static String[][] dp() throws IOException {

        String csvDataFilePath = System.getProperty("user.dir")+ File.separator+"src"+ File.separator+"test"+ File.separator+"resources"+ File.separator+"data"+ File.separator+"numberOfRandomBreed.csv";
        String[][] dataFromCSV = utils.CSVDataReaders.getCSVData(csvDataFilePath);
        return dataFromCSV;
    }
}
