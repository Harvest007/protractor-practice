package api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.given;

@RunWith(Parameterized.class)
public class MultipleRandomImageFromAllDogs {
    private String numberOfRandomImages;

    public MultipleRandomImageFromAllDogs(String numberOfRandomImages){
        this.numberOfRandomImages = numberOfRandomImages;
    }
    @Test
    public void test(){
                String urlAllDogs = "https://dog.ceo/api/breeds/image/random/"+numberOfRandomImages;
                given()
                        .get(urlAllDogs).then().statusCode(200)
                        .log().all();
    }
    @Parameterized.Parameters
    public static String[][] dp() throws IOException {

        String csvDataFilePath = System.getProperty("user.dir")+ File.separator+"src"+ File.separator+"test"+ File.separator+"resources"+ File.separator+"data"+ File.separator+"numberOfRandomImages.csv";
        String[][] dataFromCSV = utils.CSVDataReaders.getCSVData(csvDataFilePath);
        return dataFromCSV;
    }
}
