package api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.given;

@RunWith(Parameterized.class)
public class ListAllByBreed {
    private String breed;

    public ListAllByBreed(String breed){
        this.breed = breed;
    }
    @Test
    public void test(){
                String urlAllByBreed = "https://dog.ceo/api/breed/"+breed+"/images";
                given()
                        .get(urlAllByBreed).then().statusCode(200)
                        .log().all();
    }
    @Parameterized.Parameters
    public static String[][] dp() throws IOException {

        String csvDataFilePath = System.getProperty("user.dir")+ File.separator+"src"+ File.separator+"test"+ File.separator+"resources"+ File.separator+"data"+ File.separator+"listAllByBreeds.csv";
        String[][] dataFromCSV = utils.CSVDataReaders.getCSVData(csvDataFilePath);
        return dataFromCSV;
    }
}
