package api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.given;

@RunWith(Parameterized.class)
public class ListAllSubBreeds {
    private String breed;
    public ListAllSubBreeds(String breed){
        this.breed = breed;
    }
    @Test
    public void test(){
                String urlSubBreedList = "https://dog.ceo/api/breed/"+breed+"/list";
                given()
                        .get(urlSubBreedList).then().statusCode(200)
                        .log().all();
    }
    @Parameterized.Parameters
    public static String[][] dp() throws IOException {

        String csvDataFilePath = System.getProperty("user.dir")+ File.separator+"src"+ File.separator+"test"+ File.separator+"resources"+ File.separator+"data"+ File.separator+"listAllSubBreed.csv";
        String[][] dataFromCSV = utils.CSVDataReaders.getCSVData(csvDataFilePath);
        return dataFromCSV;
    }
}
