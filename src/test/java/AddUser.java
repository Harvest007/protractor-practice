import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import utils.ReusableClass;

import java.io.IOException;

@RunWith(Parameterized.class)
public class AddUser extends ReusableClass {
    private String testID;
    private String firstName;
    private String lastName;
    private String password;
    private String customer;
    private String role;
    private String email;
    private String cell;

    public AddUser(String testID,String firstName,String lastName,String password,String customer,String role,String email,String cell){
        //  super();
        this.testID = testID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.customer = customer;
        this.role = role;
        this.email = email;
        this.cell = cell;
    }
    @Before
    public void beforeTest() {
        start();
    }
    @Test
    public void addUser(){
        report("Add User",testID);
        try {
            launch();
            addUserBtn();
            addUserInput(firstName,lastName,password,customer,role,email,cell);
            confirmUserAdded(firstName,lastName);

        }catch (Exception e){
            System.out.print("The test has failed "+ e.getMessage());
        }

    }
    @Parameterized.Parameters
    public static String[][] dp() throws IOException {

        String csvDataFilePath = "src\\test\\resources\\data\\addUser.csv";
        String[][] dataFromCSV = utils.CSVDataReaders.getCSVData(csvDataFilePath);
        return dataFromCSV;
    }

    @After
    public void afterTest() {
        close();
    }
}
