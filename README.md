# Introduction
This project is for Absa Automation Assessment. the test in this project are written in java.
The test for the first task which is API testing will be stored in the api folder under java and the test for task 2 will be on the parent java folder.

We have a folder utils which has reusable class and CSVReader class used to read data files since the automation is build as TDD testing.
all our test are pointed to the resource folder path to link with its data under the folder data.

#Reporting and Screenshot
the test report data will be found under target folder along with its screenshot.
on the report we will be reporting every step where we validated the test and its status.

#Maven Projecs
Since this is a maven project please build the project before running the test, so it can pull its dependencies from the maven repository.
cmd: mvn clean install -U -DskipTests.

#Running the tests
this test are Junit test you can execute them from the @Test annotation, the project has no properties configuration... so the will not be any need to configure Vm settings.
NB: with knowledge of the data, please feel free to update the datasheet and run more test.


